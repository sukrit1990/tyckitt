package com.tyckitt.dbconnections;
// default package
// Generated Dec 31, 2015 10:16:12 PM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.tyckitt.objects.User;

/**
 * Home object for domain model class User.
 * @see .User
 * @author Hibernate Tools
 */
public class UserHome extends DefaultHome<User> {

	public UserHome(Class<User> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}
	private static final Log log = LogFactory.getLog(UserHome.class);

	public SessionFactory getSessionFactory() {
		try {
			
			Configuration config =  new Configuration();
			config.addAnnotatedClass(User.class);
			return config.configure().buildSessionFactory();

			//return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

}
