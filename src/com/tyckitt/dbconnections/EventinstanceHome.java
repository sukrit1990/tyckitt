package com.tyckitt.dbconnections;

// default package
// Generated Dec 18, 2015 12:08:10 AM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.tyckitt.objects.Eventinstance;

/**
 * Home object for domain model class Eventinstance.
 * @see .Eventinstance
 * @author Hibernate Tools
 */
public class EventinstanceHome extends DefaultHome<Eventinstance> {

	public EventinstanceHome(Class<Eventinstance> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(EventinstanceHome.class);

	public SessionFactory getSessionFactory() {
		try {
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Eventinstance.class);
			return config.configure().buildSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

}
