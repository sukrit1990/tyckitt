package com.tyckitt.dbconnections;
// Generated Dec 13, 2015 1:24:52 PM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.tyckitt.objects.Section;

/**
 * Home object for domain model class Section.
 * @see .Section
 * @author Hibernate Tools
 */
public class SectionHome extends DefaultHome<Section> {

	public SectionHome(Class<Section> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(SectionHome.class);

	public SessionFactory getSessionFactory() {
		try {
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Section.class);
					
			return config.configure().buildSessionFactory();

		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}
}
