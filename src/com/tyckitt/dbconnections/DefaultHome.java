package com.tyckitt.dbconnections;
// Generated Dec 13, 2015 1:24:52 PM by Hibernate Tools 4.3.1.Final

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

/**
 * Home object for domain model class Venue.
 * 
 * @see .Venue
 * @author Hibernate Tools
 */
public abstract class DefaultHome<RestObjectType> {

	private Class<RestObjectType> clazz;

	protected DefaultHome(Class<RestObjectType> clazz) {
		this.clazz = clazz;
	}

	private static final Log log = LogFactory.getLog(DefaultHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected abstract SessionFactory getSessionFactory();

	public void persist(RestObjectType transientInstance) {
		log.debug("persisting Venue instance");
		try {
			Transaction txn = sessionFactory.getCurrentSession().beginTransaction();
			sessionFactory.getCurrentSession().persist(transientInstance);
			txn.commit();
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(RestObjectType instance) {
		log.debug("attaching dirty Venue instance");
		try {
			Transaction txn = sessionFactory.getCurrentSession().beginTransaction();
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			txn.commit();
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(RestObjectType instance) {
		log.debug("attaching clean Venue instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	protected void delete(RestObjectType persistentInstance) {
		log.debug("deleting Venue instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public RestObjectType merge(RestObjectType detachedInstance) {
		log.debug("merging Venue instance");
		try {

			RestObjectType result = (RestObjectType) sessionFactory.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public RestObjectType findById(int id) {
		log.debug("getting Venue instance with id: " + id);
		try {

			Transaction txn = sessionFactory.getCurrentSession().beginTransaction();
			//Criteria crit = sessionFactory.getCurrentSession().createCriteria(this.clazz.getName());
			//crit.setFetchMode("venueconfigurations", FetchMode.LAZY);
			// return this.sessionFactory.getCurrentSession().createCriteria(Answer.class).add(Restrictions.like(column,"%"+keyWord+"%")).addOrder(Order.desc("retdate")).setFirstResult((currentPage-1)*lineSize).setMaxResults(lineSize).list();

			RestObjectType instance = (RestObjectType) sessionFactory.getCurrentSession().get(this.clazz.getName(), id);
			txn.commit();
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<RestObjectType> findByExample(RestObjectType instance) {
		log.debug("finding Venue instance by example");
		try {
			Transaction txn = sessionFactory.getCurrentSession().beginTransaction();
			List<RestObjectType> results = sessionFactory.getCurrentSession().createCriteria(this.clazz.getName())
					.add(Example.create(instance)).list();
			txn.commit();
			log.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
