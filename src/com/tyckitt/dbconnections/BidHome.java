package com.tyckitt.dbconnections;

// default package
// Generated Dec 18, 2015 12:08:10 AM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.tyckitt.objects.Bid;

/**
 * Home object for domain model class Bid.
 * @see .Bid
 * @author Hibernate Tools
 */
public class BidHome extends DefaultHome<Bid> {

	public BidHome(Class<Bid> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(BidHome.class);

	public SessionFactory getSessionFactory() {
		try {
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Bid.class);
					
			return config.configure().buildSessionFactory();

		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}
}
