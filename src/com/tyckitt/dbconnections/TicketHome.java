package com.tyckitt.dbconnections;
// default package
// Generated Dec 31, 2015 10:16:12 PM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import com.tyckitt.objects.Ticket;

/**
 * Home object for domain model class Ticket.
 * @see .Ticket
 * @author Hibernate Tools
 */
public class TicketHome extends DefaultHome<Ticket>{

	public TicketHome(Class<Ticket> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(TicketHome.class);

	public SessionFactory getSessionFactory() {
		try {
			
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Ticket.class);
			return config.configure().buildSessionFactory();

			//return (SessionFactory) new InitialContext().lookup("SessionFactory");
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}

	
}
