package com.tyckitt.dbconnections;

// default package
// Generated Dec 18, 2015 12:08:10 AM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.tyckitt.objects.Eventinstancesection;

/**
 * Home object for domain model class Eventinstancesection.
 * @see .Eventinstancesection
 * @author Hibernate Tools
 */
public class EventinstancesectionHome extends DefaultHome<Eventinstancesection>{

	public EventinstancesectionHome(Class<Eventinstancesection> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(EventinstancesectionHome.class);

	public SessionFactory getSessionFactory() {
		try {
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Eventinstancesection.class);
			return config.configure().buildSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}
}
