package com.tyckitt.dbconnections;

// default package
// Generated Dec 20, 2015 1:23:39 AM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.tyckitt.objects.Seat;

/**
 * Home object for domain model class Seat.
 * @see .Seat
 * @author Hibernate Tools
 */
public class SeatHome extends DefaultHome<Seat>{

	public SeatHome(Class<Seat> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(SeatHome.class);

	@Override
	public SessionFactory getSessionFactory() {
		try {
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Seat.class);
			return config.configure().buildSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}



}
