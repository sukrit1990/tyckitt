package com.tyckitt.dbconnections;
// Generated Dec 13, 2015 1:24:52 PM by Hibernate Tools 4.3.1.Final

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.tyckitt.objects.Identity;

/**
 * Home object for domain model class Row.
 * @see .Row
 * @author Hibernate Tools
 */
public class IdentityHome extends DefaultHome<Identity>{

	public IdentityHome(Class<Identity> clazz) {
		super(clazz);
		// TODO Auto-generated constructor stub
	}

	private static final Log log = LogFactory.getLog(IdentityHome.class);

	public SessionFactory getSessionFactory() {
		try {
			Configuration config =  new Configuration();
			config.addAnnotatedClass(Identity.class);
					
			return config.configure().buildSessionFactory();

		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException("Could not locate SessionFactory in JNDI");
		}
	}
}
