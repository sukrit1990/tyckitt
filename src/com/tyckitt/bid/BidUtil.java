package com.tyckitt.bid;

import java.util.Calendar;

import com.tyckitt.objects.Bid;
import com.tyckitt.objects.BidStatus;

public class BidUtil {

	public static void populateBidDetails(Bid bid) {
		bid.setStatus(BidStatus.ACTIVE.name());
		
		//get current date time with Calendar()
		Calendar cal = Calendar.getInstance();
		bid.setTimeplaced(cal.getTime());
	}
	
	

}
