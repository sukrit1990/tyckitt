package com.tyckitt.common;

import javax.ws.rs.core.Response;

import com.tyckitt.objects.ResponseCode;
import com.tyckitt.objects.TyckittResponse;

public class ResponseBuilder {
	
	public static Response deny(String message){
		
		TyckittResponse response = new TyckittResponse();
		response.setReason(message);
		response.setResponseCode(ResponseCode.DENY);
		return Response.status(200).entity(JsonMapper.mapObjectToJson(response)).build();
	}

}
