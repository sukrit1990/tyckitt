package com.tyckitt.login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.UserHome;
import com.tyckitt.objects.User;

public class LoginHelper {

	public static final String SALT = "my-salt-text";
	DefaultHome<User> home = new UserHome(User.class);
	private User user;

	public Boolean login(String email, String password) {
		Boolean isAuthenticated = false;

		User user = new User();
		user.setEmail(email);
		// remember to use the same SALT value use used while storing password
		// for the first time.
		String saltedPassword = SALT + password;
		String hashedPassword = generateHash(saltedPassword);

		List<User> users = home.findByExample(user);

		if (users == null || users.size() == 0) {
			return false;
		}

		System.out.println("DB password is " + users.get(0).getPasswordh());
		System.out.println("hashed password is " + hashedPassword);

		// There should be only one user
		if (hashedPassword.equals(users.get(0).getPasswordh())) {
			isAuthenticated = true;
			this.setUser(users.get(0));
		} else {
			isAuthenticated = false;
		}
		return isAuthenticated;
	}

	public String getHashed(String password) {
		String saltedPassword = SALT + password;
		String hashedPassword = generateHash(saltedPassword);
		return hashedPassword;
	}

	private String generateHash(String input) {
		StringBuilder hash = new StringBuilder();

		try {
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(input.getBytes());
			char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
			for (int idx = 0; idx < hashedBytes.length; idx++) {
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}
		} catch (NoSuchAlgorithmException e) {
			// handle error here.
		}

		return hash.toString();
	}

	public User getUser() {
		return user;
	}

	private void setUser(User user) {
		this.user = user;
	}

}
