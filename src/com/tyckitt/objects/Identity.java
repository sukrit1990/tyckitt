package com.tyckitt.objects;
// default package
// Generated Jan 9, 2016 6:24:20 PM by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Identity generated by hbm2java
 */

@Entity
public class Identity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String email;
	
	@ManyToOne
	@Cascade(CascadeType.MERGE)
	@JoinColumn(name = "user_id")
	private User user;
	
	private String provider;
	private String name;
	
	@Column(name = "access_token")
	private String accessToken;
	
	@Column(name = "refresh_token")
	private String refreshToken;
	
	@Id
	@Column(name = "uid", unique = true, nullable = false)
	private String uid;
	
	@Column(name = "expires_at")
	private Integer expiresAt;

	public Identity() {
	}

	public Identity(String email, User user) {
		this.email = email;
		this.user = user;
	}

	public Identity(String email, User user, String provider, String name, String accessToken, String refreshToken,
			String uid, Integer expiresAt) {
		this.email = email;
		this.user = user;
		this.provider = provider;
		this.name = name;
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.uid = uid;
		this.expiresAt = expiresAt;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getProvider() {
		return this.provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAccessToken() {
		return this.accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return this.refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Integer getExpiresAt() {
		return this.expiresAt;
	}

	public void setExpiresAt(Integer expiresAt) {
		this.expiresAt = expiresAt;
	}

}
