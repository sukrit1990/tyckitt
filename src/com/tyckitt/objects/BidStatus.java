package com.tyckitt.objects;

public enum BidStatus {

	ACTIVE("ACTIVE"),
	
	ESCALATED("ESCALATED"),
	
	EXPIRED("EXPIRED");
	
	private String persistentValue;


	/**
	 * @param value
	 */
	private BidStatus(String persistentValue) {
		this.persistentValue = persistentValue;
	}


	public String getPersistentValue() {
		return this.persistentValue;
	}
	
	/**
	 * Gets the enum name by value.
	 *
	 * @param value the value
	 * @return the enum name by value
	 */
	public static BidStatus getEnumNameByValue(String value) {
		for (BidStatus e : BidStatus.values()) {
			if (e.getPersistentValue().equals(value)) {
				return e;
			}
		}
		return null;
	}

	
	public String getValue() {
		return this.name();
	}
}
