package com.tyckitt.objects;

public enum TicketStatus {

	BOOKED("BOOKED"),
	
	CONFIRMED("CONFIRMED"),
	
	CANCELLED("CANCELLED"),
	
	EXPIRED("EXPIRED");;
	
	private String persistentValue;


	/**
	 * @param value
	 */
	private TicketStatus(String persistentValue) {
		this.persistentValue = persistentValue;
	}


	public String getPersistentValue() {
		return this.persistentValue;
	}
	
	/**
	 * Gets the enum name by value.
	 *
	 * @param value the value
	 * @return the enum name by value
	 */
	public static TicketStatus getEnumNameByValue(String value) {
		for (TicketStatus e : TicketStatus.values()) {
			if (e.getPersistentValue().equals(value)) {
				return e;
			}
		}
		return null;
	}

	
	public String getValue() {
		return this.name();
	}
}
