package com.tyckitt.interfaces;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.common.JsonMapper;
import com.tyckitt.dbconnections.CurrencyHome;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.objects.Currency;

@Path("/currency")
public class CurrencyInterface {

	DefaultHome<Currency> home = new CurrencyHome(Currency.class);


	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyRESTService(@QueryParam("name") String name, @QueryParam("id") int id) {
		Currency currency = new Currency();
		List<Currency> currencies = new ArrayList<Currency>();
		System.out.println("id is" + id);
		
		if (id!=0) {
			currency.setId(id);
			currencies.add(home.findById(id));
			
		}
		else {
			currency.setName(name);
			currencies = home.findByExample(currency);
			
		}
		
		//DatabaseConnector
		return Response.status(200).entity(JsonMapper.mapObjectToJson(currencies)).build();
	}

}
