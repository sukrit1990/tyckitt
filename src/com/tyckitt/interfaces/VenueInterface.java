package com.tyckitt.interfaces;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.common.JsonMapper;
import com.tyckitt.common.PATCH;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.RowHome;
import com.tyckitt.dbconnections.SeatHome;
import com.tyckitt.dbconnections.SectionHome;
import com.tyckitt.dbconnections.VenueHome;
import com.tyckitt.dbconnections.VenueconfigurationHome;
import com.tyckitt.objects.Row;
import com.tyckitt.objects.Seat;
import com.tyckitt.objects.Section;
import com.tyckitt.objects.Venue;
import com.tyckitt.objects.Venueconfiguration;

@Path("/venue")
public class VenueInterface {
	
	DefaultHome<Venue> home = new VenueHome(Venue.class);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyRESTService(@QueryParam("name") String name, @QueryParam("id") int id) {
		Venue venue = new Venue();
		List<Venue> venues = new ArrayList<Venue>();
		System.out.println("id is" + id);
		
		if (id!=0) {
			venue.setId(id);
			venues.add(home.findById(id));
			
		}
		else {
			venue.setName(name);
			venues = home.findByExample(venue);
			
		}
		
		//DatabaseConnector
		return Response.status(200).entity(JsonMapper.mapObjectToJson(venues)).build();
	}
	
	/*Create a new venue and return the venue id in response*/
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST( Venue venue) {
		
		home.persist(venue);
		
		
		if (!venue.getVenueconfigurations().isEmpty()) {
			Iterator<Venueconfiguration> itr = venue.getVenueconfigurations().iterator() ;
			while (itr.hasNext()) {
				DefaultHome<Venueconfiguration> configHome=new VenueconfigurationHome(Venueconfiguration.class);
				Venueconfiguration configuration = itr.next();
				System.out.println("venue id is " + venue.getId());
				configuration.setVenue(venue);
				configHome.persist(configuration);
				
				if (!configuration.getSections().isEmpty()) {
					Iterator<Section> itrSection = configuration.getSections().iterator() ;
					while (itrSection.hasNext()) {
						DefaultHome<Section> sectionHome=new SectionHome(Section.class);
						Section section = itrSection.next();
						System.out.println("venueconfiguration id is " + configuration.getId());
						section.setVenueconfiguration(configuration);
						sectionHome.persist(section);
						
						if (!section.getRows().isEmpty()) {
							Iterator<Row> itrRow = section.getRows().iterator() ;
							while (itrRow.hasNext()) {
								DefaultHome<Row> rowHome=new RowHome(Row.class);
								Row row = itrRow.next();
								System.out.println("section id is " + section.getId());
								row.setSection(section);
								rowHome.persist(row);
								
								if (!row.getSeats().isEmpty()) {
									Iterator<Seat> itrSeat = row.getSeats().iterator() ;
									while (itrSeat.hasNext()) {
										DefaultHome<Seat> seatHome=new SeatHome(Seat.class);
										Seat seat = itrSeat.next();
										System.out.println("row id is " + row.getId());
										seat.setRow(row);
										seatHome.persist(seat);
										
										System.out.println("seat id is " + seat.getId());
										
									
									}
								}
							
							}
						}
					
					}
				}
			
			}
		}
		
		return Response.status(200).entity(JsonMapper.mapObjectToJson(venue)).build();
	}
	
	
	/*Create a new venue and return the venue id in response*/
	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	public Response patchVenue( Venue venue) {
		
		home.attachDirty(venue);
		
		
		if (!venue.getVenueconfigurations().isEmpty()) {
			Iterator<Venueconfiguration> itr = venue.getVenueconfigurations().iterator() ;
			while (itr.hasNext()) {
				DefaultHome<Venueconfiguration> configHome=new VenueconfigurationHome(Venueconfiguration.class);
				Venueconfiguration configuration = itr.next();
				System.out.println("venue id is " + venue.getId());
				configuration.setVenue(venue);
				configHome.attachDirty(configuration);
				
				if (!configuration.getSections().isEmpty()) {
					Iterator<Section> itrSection = configuration.getSections().iterator() ;
					while (itrSection.hasNext()) {
						DefaultHome<Section> sectionHome=new SectionHome(Section.class);
						Section section = itrSection.next();
						System.out.println("venueconfiguration id is " + configuration.getId());
						section.setVenueconfiguration(configuration);
						sectionHome.attachDirty(section);
						
						if (!section.getRows().isEmpty()) {
							Iterator<Row> itrRow = section.getRows().iterator() ;
							while (itrRow.hasNext()) {
								DefaultHome<Row> rowHome=new RowHome(Row.class);
								Row row = itrRow.next();
								System.out.println("section id is " + section.getId());
								row.setSection(section);
								rowHome.attachDirty(row);
								
								if (!row.getSeats().isEmpty()) {
									Iterator<Seat> itrSeat = row.getSeats().iterator() ;
									while (itrSeat.hasNext()) {
										DefaultHome<Seat> seatHome=new SeatHome(Seat.class);
										Seat seat = itrSeat.next();
										System.out.println("row id is " + row.getId());
										seat.setRow(row);
										seatHome.attachDirty(seat);
										
										System.out.println("seat id is " + seat.getId());
										
									
									}
								}
								
							
							}
						}
					
					}
				}
			
			}
		}
		
		return Response.status(200).entity(JsonMapper.mapObjectToJson(venue)).build();
	}

}
