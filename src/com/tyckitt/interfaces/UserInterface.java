package com.tyckitt.interfaces;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.common.JsonMapper;
import com.tyckitt.common.PATCH;
import com.tyckitt.common.ResponseBuilder;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.UserHome;
import com.tyckitt.login.LoginHelper;
import com.tyckitt.objects.User;

@Path("/user")
public class UserInterface {

	DefaultHome<User> home = new UserHome(User.class);

	/*
	 * This method is used for logging in a user
	 */

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response loginUser(@QueryParam("email") String email, @QueryParam("password") String password) {

		if (email.isEmpty() || password.isEmpty()) {
			return ResponseBuilder.deny("Email/Password is emapty");
		}

		if (!UserInterface.isValidEmailAddress(email)) {
			return ResponseBuilder.deny("Invalid email");
		}

		LoginHelper helper = new LoginHelper();

		if (!helper.login(email, password)) {
			return ResponseBuilder.deny("Invalid email or password");
		} else {
			User user = helper.getUser();
			return Response.status(200).entity(JsonMapper.mapObjectToJson(user)).build();
		}

	}

	/*
	 * This method is used for getting user details by id
	 */

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@QueryParam("id") int id) {

		if (id == 0) {
			return ResponseBuilder.deny("User id is emapty");
		}

		User user = home.findById(id);

		if (user == null) {
			return ResponseBuilder.deny("No such user");
		} else {
			return Response.status(200).entity(JsonMapper.mapObjectToJson(user)).build();
		}

	}

	/*
	 * Sign up a new user
	 */

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createUser(User user) {

		if (user.getEmail().isEmpty()) {
			return ResponseBuilder.deny("Email can't be null");
		}

		if (user.getPasswordh().isEmpty()) {
			return ResponseBuilder.deny("password can't be null");
		}

		if (user.getName().isEmpty()) {
			return ResponseBuilder.deny("name can't be null");
		}

		LoginHelper helper = new LoginHelper();
		user.setPasswordh(helper.getHashed(user.getPasswordh()));
		user.setIdentities(null); // To avoid adding identities through this api
		user.setTickets(null); // To avoid adding tickets through this api
		try {
			home.persist(user);
		} catch (Exception e) {
			return ResponseBuilder.deny(e.getLocalizedMessage());
		}
		return Response.status(200).entity(JsonMapper.mapObjectToJson(user)).build();
	}

	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(User user) {

		if (user.getId() == 0) {
			return ResponseBuilder.deny("Id can't be empty");
		}

		if (user.getPasswordh() != null) {
			LoginHelper helper = new LoginHelper();
			user.setPasswordh(helper.getHashed(user.getPasswordh()));
		}
		user.setIdentities(null); // To avoid adding identities through this api
		user.setTickets(null); // To avoid adding tickets through this api
		try {
			
			home.attachDirty(user);
		} catch (Exception e) {
			return ResponseBuilder.deny(e.getLocalizedMessage());
		}
		return Response.status(200).entity(JsonMapper.mapObjectToJson(user)).build();
	}

	private static boolean isValidEmailAddress(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

}
