package com.tyckitt.interfaces;

import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.common.JsonMapper;
import com.tyckitt.common.PATCH;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.RowHome;
import com.tyckitt.dbconnections.SectionHome;
import com.tyckitt.dbconnections.VenueconfigurationHome;
import com.tyckitt.objects.Row;
import com.tyckitt.objects.Section;
import com.tyckitt.objects.Venueconfiguration;

@Path("/venueconfiguration")
public class VenueConfigurationInterface {

	DefaultHome<Venueconfiguration> configHome = new VenueconfigurationHome(Venueconfiguration.class);
	
	
	/*Create a new venue and return the venue id in response*/
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST( Venueconfiguration configuration) {
		
			configHome.persist(configuration);
				
				if (!configuration.getSections().isEmpty()) {
					Iterator<Section> itrSection = configuration.getSections().iterator() ;
					while (itrSection.hasNext()) {
						DefaultHome<Section> sectionHome=new SectionHome(Section.class);
						Section section = itrSection.next();
						System.out.println("venueconfiguration id is " + configuration.getId());
						section.setVenueconfiguration(configuration);
						sectionHome.persist(section);
						
						if (!section.getRows().isEmpty()) {
							Iterator<Row> itrRow = section.getRows().iterator() ;
							while (itrRow.hasNext()) {
								DefaultHome<Row> rowHome=new RowHome(Row.class);
								Row row = itrRow.next();
								System.out.println("section id is " + section.getId());
								row.setSection(section);
								rowHome.persist(row);
								
								System.out.println("row id is " + row.getId());
								
							
							}
						}
					
					}
				}
			
		
				return Response.status(200).entity(JsonMapper.mapObjectToJson(configuration)).build();
	}
	
}
