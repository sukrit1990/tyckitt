package com.tyckitt.interfaces;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.bid.BidUtil;
import com.tyckitt.common.JsonMapper;
import com.tyckitt.dbconnections.BidHome;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.EventinstancesectionHome;
import com.tyckitt.objects.Bid;
import com.tyckitt.objects.Eventinstancesection;
import com.tyckitt.objects.ResponseCode;
import com.tyckitt.objects.TyckittResponse;

@Path("/bid")
public class BidInterface {
	
	DefaultHome<Bid> home = new BidHome(Bid.class);
	DefaultHome<Eventinstancesection> eventInstanceSectionHome = new EventinstancesectionHome(Eventinstancesection.class);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyRESTService(@QueryParam("id") int id) {
		Bid bid = new Bid();
		List<Bid> bids = new ArrayList<Bid>();
		System.out.println("id is" + id);
		
		
			bid.setId(id);
			bids.add(home.findById(id));
		
		
		//DatabaseConnector
		return Response.status(200).entity(JsonMapper.mapObjectToJson(bids)).build();
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST( Bid bid) {
		
		TyckittResponse response = new TyckittResponse();
		
		//validate if section id is not populated
		
		//check if currency conversion is needed
	
		//get section details by id
		Eventinstancesection instanceSection = eventInstanceSectionHome.findById(bid.getEventinstancesection().getId());
		
		//check whether bid value > minimum acceptable bid
		if (bid.getValue() < instanceSection.getMimimumacceptablebid()) {
			response.setResponseCode(ResponseCode.DENY);
			response.setReason("Minimum bid value is not satisfied");
			return Response.status(200).entity(JsonMapper.mapObjectToJson(response)).build();
		}
		
		//check whether date of expiry has crossed
		
		//populate bid object
		BidUtil.populateBidDetails(bid);
		
		home.persist(bid);
		
		//update minimum acceptable bid
		
		//place automated bids
		
		
		return Response.status(200).entity(JsonMapper.mapObjectToJson(response)).build();
	}

}
