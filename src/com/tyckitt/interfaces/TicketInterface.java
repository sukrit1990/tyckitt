package com.tyckitt.interfaces;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.common.JsonMapper;
import com.tyckitt.common.PATCH;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.TicketHome;
import com.tyckitt.dbconnections.UserHome;
import com.tyckitt.objects.Ticket;
import com.tyckitt.objects.TicketStatus;
import com.tyckitt.objects.User;

@Path("/ticket")
public class TicketInterface {

	DefaultHome<Ticket> home = new TicketHome(Ticket.class);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyRESTService(@QueryParam("id") int id) {
		Ticket ticket = new Ticket();
		List<Ticket> tickets = new ArrayList<Ticket>();

		ticket.setId(id);
		tickets.add(home.findById(id));

		// DatabaseConnector
		return Response.status(200).entity(JsonMapper.mapObjectToJson(tickets)).build();
	}

	/* Create a new ticket and return the ticket id in response */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST(Ticket ticket) {

		//Check if combination of seat and eventinstancesection id not already taken
		
		//if seat is not null, check if seat id corresponds to eventinstancesection
		
		DefaultHome<User> userHome = new UserHome(User.class);
		userHome.persist(ticket.getUser());
		
		//Set ticket status
		ticket.setStatus(TicketStatus.BOOKED.name());
		
		home.persist(ticket);
		
		//if seat is null, allocate random seat

		return Response.status(200).entity(JsonMapper.mapObjectToJson(ticket)).build();
	}

	/* Update the ticket */
	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	public Response patchTicket(Ticket ticket) {

		home.attachDirty(ticket);

		return Response.status(200).entity(JsonMapper.mapObjectToJson(ticket)).build();
	}

}
