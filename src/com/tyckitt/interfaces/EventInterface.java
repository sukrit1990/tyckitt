package com.tyckitt.interfaces;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tyckitt.common.JsonMapper;
import com.tyckitt.dbconnections.DefaultHome;
import com.tyckitt.dbconnections.EventHome;
import com.tyckitt.dbconnections.EventinstanceHome;
import com.tyckitt.dbconnections.EventinstancesectionHome;
import com.tyckitt.event.EventUtils;
import com.tyckitt.objects.Event;
import com.tyckitt.objects.Eventinstance;
import com.tyckitt.objects.Eventinstancesection;

@Path("/event")
public class EventInterface {
	
	DefaultHome<Event> home = new EventHome(Event.class);
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response verifyRESTService(@QueryParam("name") String name, @QueryParam("id") int id) {
		Event event = new Event();
		List<Event> events = new ArrayList<Event>();
		System.out.println("id is" + id);
		
		if (id!=0) {
			event.setId(id);
			events.add(home.findById(id));
			
		}
		else {
			event.setName(name);
			events = home.findByExample(event);
			
		}
		
		//DatabaseConnector
		return Response.status(200).entity(JsonMapper.mapObjectToJson(events)).build();
	}
	
	/*Create a new venue and return the venue id in response*/
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crunchifyREST( Event event) {
		
		//DefaultHome<Event> home = new EventHome(Event.class);
		home.persist(event);
		
		
		if (!event.getEventinstances().isEmpty()) {
			Iterator<Eventinstance> itr = event.getEventinstances().iterator() ;
			while (itr.hasNext()) {
				DefaultHome<Eventinstance> instanceHome=new EventinstanceHome(Eventinstance.class);
				Eventinstance instance = itr.next();
				System.out.println("venue id is " + event.getId());
				instance.setEvent(event);
				instanceHome.persist(instance);
				
				if (!instance.getEventinstancesections().isEmpty()) {
					Iterator<Eventinstancesection> itrSection = instance.getEventinstancesections().iterator() ;
					while (itrSection.hasNext()) {
						EventinstancesectionHome sectionHome=new EventinstancesectionHome(Eventinstancesection.class);
						Eventinstancesection section = itrSection.next();
						EventUtils.populateBidDetails(section);
						section.setEventinstance(instance);
						sectionHome.persist(section);
					
					}
				}
			
			}
		}
		
		return Response.status(200).entity(JsonMapper.mapObjectToJson(event)).build();
	}

}
